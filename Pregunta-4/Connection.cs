using System;
using System.Data;
using System.Data.SqlClient;

namespace DatabaseConnection
{
    public class Database
    {
        private SqlConnection connection;
        private string connectionString;

        public Database(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void AbrirConexion()
        {
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        public void CerrarConexion()
        {
            connection.Close();
        }
        //Ejecutar Select
        public DataSet EjecutarSelect(string query)
        {
            DataSet dataSet = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
            adapter.Fill(dataSet);
            return dataSet;
        }


        //Ejecutar insert, udpate o delete
        public int EjecutarOtros(string query)
        {
            SqlCommand command = new SqlCommand(query, connection);
            return command.ExecuteNonQuery();
        }

        //Numero de filas retornadas, o filas afectadas para cualquier Query
        public int ExecuteScalar(string query)
        {
            SqlCommand command = new SqlCommand(query, connection);
            return Convert.ToInt32(command.ExecuteScalar());
        }
    }
}
