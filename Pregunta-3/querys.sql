--  A.	Escriba la consulta en SQL que devuelva el nombre del proyecto y sus productos correspondientes del proyecto premia 
--  cuyo código es 1
SELECT py.NOMBRE AS Proyecto, pr.DESCRIPCION AS Producto 
FROM PROYECTO py
INNER JOIN PRODUCTO_PROYECTO pp ON pp.PROYECTO = py.PROYECTO 
INNER JOIN PRODUCTO pr ON pp.PRODUCTO= pr.PRODUCTO
WHERE py.PROYECTO = 1;
//
-- B.	Escriba una consulta SQL que devuelva los distintos mensajes que hay, 
--     indicando a qué proyecto y producto pertenecen.
SELECT msj.COD_MENSAJE, py.NOMBRE AS Proyecto, pr.DESCRIPCION AS Producto 
FROM 
    MENSAJE msj
INNER JOIN PRODUCTO_PROYECTO pp 
    ON pp.PROYECTO = msj.PROYECTO AND pp.PRODUCTO= msj.PRODUCTO
INNER JOIN PROYECTO py ON pp.PROYECTO = py.PROYECTO
INNER JOIN PRODUCTO pr ON pp.PRODUCTO= pr.PRODUCTO
//

-- C. Escriba una consulta SQL que devuelva los distintos mensajes que hay,
--  indicando a qué proyecto y producto pertenecen. Pero si el mensaje está en todos los productos de un proyecto, en lugar de mostrar cada producto, debe mostrar el nombre del proyecto y un solo producto que diga “TODOS”.
SELECT 	
	DISTINCT
	msj.COD_MENSAJE as mensaje, 
    py.NOMBRE as proyecto, 
    CASE  
		WHEN (select count(*) from MENSAJE where PROYECTO= PROYECTO.PROYECTO) 
           = (select count(*) from PRODUCTO_PROYECTO where PRODUCTO_PROYECTO.PROYECTO= PROYECTO.PROYECTO) 
           then 'Todos'
        ELSE 
		    pr.DESCRIPCION
	END as produco
FROM 
    MENSAJE msj
INNER JOIN PRODUCTO_PROYECTO pp 
    ON pp.PROYECTO = msj.PROYECTO AND pp.PRODUCTO= msj.PRODUCTO
INNER JOIN PROYECTO py ON pp.PROYECTO = py.PROYECTO
INNER JOIN PRODUCTO pr ON pp.PRODUCTO= pr.PRODUCTO