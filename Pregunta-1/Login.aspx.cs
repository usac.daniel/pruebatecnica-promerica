using System;
using System.Web.UI;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string username = Request.Form["Username"];
                string password = Request.Form["Password"];

                if (username == "admin" && password == "password")
                {
                    // Redirect to the main page
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    // Show an error message
                    Response.Write("Invalid username or password.");
                }
            }
        }
    }
}
